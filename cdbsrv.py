#!/usr/bin/env python
# -*- mode: python; coding: iso-8859-1 -*-
#
# Copyright (C) 1990 - 2012 CONTACT Software GmbH
# All rights reserved.
# http://www.contact.de/
#
"""
Worker process implementation for application server sessions.
"""

__docformat__ = "restructuredtext en"
__revision__ = "$Id: cdbsrv.xpy 113366 2014-08-13 14:40:56Z sla $"

# Some imports
import sys
from cdb.scripts import cdbsrv

if __name__ == "__main__":
    sys.exit(cdbsrv.main())
